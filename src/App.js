import React from 'react'
import HomePage from './routes/HomePage/HomePage'
import AppBar from './components/AppBar/AppBar'
import Footer from './components/Footer/Footer'
import News from './routes/NewsPage/News'
//import OurDogs from './components/OurDogs'
import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { HashLink as Link } from 'react-router-hash-link'

const App = () => {
  
  const news = [{title:"otsikko", text:"rrrr", date:"2.2.2021"}, {title:"otsikko", text:"rrrr dddd ddddd", date:"5.5.2021"}]

  return (
    <Router>
      <AppBar Link={Link}></AppBar>
      <Routes>
        <Route path="/" element={<HomePage Link={Link}></HomePage>}/>
        <Route path="/uutisia" element={<News news={news}></News>}/>
      </Routes>
      <Footer></Footer>
    </Router>
  )
}


//<Route path="/koiramme" element={<OurDogs></OurDogs>}/>
export default App