import React from 'react'
import './Footer.css'

const Footer = () => {
  return (
    <div className='footer'>
      <div className='footer-text'>
        ©️ Hunajakuun
      </div>
    </div>
  )
}

export default Footer