import React from 'react'
import './AppBar.css'
import AppBarLogic from './AppBarLogic'

const AppBar = ({ Link }) => {

  const {sideBarOpen, handleButtonClick, selectedAppbarList, selectedMobileNavbarList} = AppBarLogic()

  if (sideBarOpen === false) {
    return (
      <div className='a1' id='top'>
        <div className='appbar-logo-container'>
          <div className='appbar-logo'>
          </div>
          <h2 className='appbar-logo-text'>
            Hunajakuun
          </h2>
        </div>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
        <button className="btn" onClick={handleButtonClick}><i className="fa fa-bars"></i></button>
        <nav className='appbar-content'>
          <Link className='appbar-button' smooth to="/"><button className={selectedAppbarList[0]}>Etusivu</button></Link>
        </nav>
        <nav className='appbar-content'>
          <Link className='appbar-button' smooth to="/#meista"><button className={selectedAppbarList[1]}>Meistä</button></Link>
        </nav>
        <nav className='appbar-content'>
          <Link className='appbar-button' smooth to="/uutisia"><button className={selectedAppbarList[2]}>Uutisia</button></Link>
        </nav>
        <nav className='appbar-content'>
          <Link className='appbar-button' smooth to="/koiramme"><button className={selectedAppbarList[3]}>Koiramme</button></Link>
        </nav>
      </div>
    )
  } else {
    return (
      <div>
        <div>
          <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"></link>
          <button className="cancel-btn" onClick={() => handleButtonClick()}><i className="fa fa-times"></i></button>
        </div>
        <div>
          <nav className='mobile-navbar-row-odd'><Link smooth to="/"><button className={selectedMobileNavbarList[0]} onClick={() => handleButtonClick()}>Etusivu</button></Link></nav>
          <nav className='mobile-navbar-row-even'><Link smooth to="/#meista"><button className={selectedMobileNavbarList[1]} onClick={() => handleButtonClick()}>Meistä</button></Link></nav>
          <nav className='mobile-navbar-row-odd'><Link smooth to="/uutisia"><button className={selectedMobileNavbarList[2]} onClick={() => handleButtonClick()}>Uutisia</button></Link></nav>
          <nav className='mobile-navbar-row-even'><Link smooth to="/koiramme"><button className={selectedMobileNavbarList[3]} onClick={() => handleButtonClick()}>Koiramme</button></Link></nav>
          <div className='mobile-navbar-margin-bottom'></div>
        </div>
      </div>
    )
  }
}

export default AppBar