import { useState } from 'react'
import { useLocation } from 'react-router-dom'

const AppBarLogic = () => {
  const [sideBarOpen, setSideBarOpen] = useState(false)
  const location = useLocation();
  let selectedAppbarList = []
  let selectedMobileNavbarList = []

  for (let x = 0; x < 4; x++) {
    selectedAppbarList[x] = 'appbar-button'
    selectedMobileNavbarList[x] = 'mobile-navbar-button'
  }

  if (location.pathname === '/') {
    selectedAppbarList[0] = 'appbar-button-selected'
    selectedMobileNavbarList[0] = 'mobile-navbar-button-selected'
  } else if (location.pathname === '/uutisia' || location.pathname === '/uutisia/') {
    selectedAppbarList[2] = 'appbar-button-selected'
    selectedMobileNavbarList[2] = 'mobile-navbar-button-selected'
  } else if (location.pathname === '/koiramme' || location.pathname === '/koiramme/') {
    selectedAppbarList[3] = 'appbar-button-selected'
    selectedMobileNavbarList[3] = 'mobile-navbar-button-selected'
  }

  const handleButtonClick = () => {
    sideBarOpen ? setSideBarOpen(false) : setSideBarOpen(true)
  }

  return{sideBarOpen, handleButtonClick, selectedAppbarList, selectedMobileNavbarList}
}

export default AppBarLogic