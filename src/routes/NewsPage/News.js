import React from 'react'
import './News.css'
import imageUrl1 from '../../img/knitting.jpg'
import imageUrl2 from '../../img/koera.jpg'

const News = ({ news }) => {

  return (
    <div className='news-page'>
      {news.map((newsObject) =>
      <div className='news-element' key={newsObject.title + newsObject.date}>
        <div className='news-element-title'>
          {newsObject.title}
        </div>
        <p className='news-element-text'>
          {newsObject.text}
        </p>
        <div className='news-element-date'>
          {newsObject.date}
        </div>
        <div className='news-element-images'>
          <div className='news-element-image' style={{backgroundImage:`url(${imageUrl1})`}} />
          <div className='news-element-second-image' style={{backgroundImage:`url(${imageUrl2})`}}/>
        </div>
      </div>
      )}
    </div>
  )
}

export default News