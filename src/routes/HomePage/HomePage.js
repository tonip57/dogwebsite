import React from 'react'
import './HomePage.css'
import nayttelyKuva from '../../img/knitting.jpg'
import uutisiaKuva from '../../img/koera.jpg'
import kuviaKuva from '../../img/mainimage3.png'

const HomePage = ({ Link }) => {

  return (
    <div className='grid-container'>
        <div className='a2'>
          <div className='a2-content'>
            <h2 className='a2-content-title'>
              Koiria
            </h2>
            <h4 className='a2-content-text'>
              Koirien kasvatusta
            </h4>
          </div>
        </div>
        <div className='a8'>
          <div className='a8-content'>
            <div className='a8-product'>
              <Link smooth to="/uutisia/#top"><button className='a8-image-button' style={{backgroundImage:`linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(${nayttelyKuva})`}}><p className="a8-text">UUTISIA</p></button></Link>
            </div>
            <div className='a8-product'>
              <Link smooth to="/koiramme/#top"><button className='a8-image-button' style={{backgroundImage:`linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(${uutisiaKuva})`}}><p className="a8-text">KOIRAMME</p></button></Link>
            </div>
            <div className='a8-product'>
              <button className='a8-image-button' style={{backgroundImage:`linear-gradient(rgba(0, 0, 0, 0.2),rgba(0, 0, 0, 0.2)),url(${kuviaKuva})`}}><p className="a8-text">KASVATIT</p></button>
            </div>
          </div>
        </div>
        <div className='a9'>
          <div className='a9-content'>
            <h3 className='a9-title' id='meista'>
              Meistä
            </h3>
            <p className='a9-text'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Duis sem libero, hendrerit a purus et, dapibus semper mauris. Nunc tristique facilisis nisi, eu tempus lacus pellentesque at. Fusce fermentum lobortis sapien, ac pharetra magna tincidunt at. Nulla suscipit lectus quis sapien porttitor, at accumsan urna porta. Nunc faucibus posuere tortor a rutrum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam condimentum nisl non purus fermentum efficitur. Donec et semper erat. Pellentesque quis malesuada neque. Vestibulum tempor, eros eu iaculis cursus, purus ante mollis risus, vel dapibus sapien turpis tempus sapien. Phasellus sodales tortor eget tristique rhoncus.
            </p>
            <h3 className='a9-title'>
              Ota yhteyttä
            </h3>
            <p className='a9-text'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Duis sem libero, hendrerit a purus et, dapibus semper mauris. Nunc tristique facilisis nisi, eu tempus lacus pellentesque at. Fusce fermentum lobortis sapien, ac pharetra magna tincidunt at. Nulla suscipit lectus quis sapien porttitor, at accumsan urna porta. Nunc faucibus posuere tortor a rutrum. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Etiam condimentum nisl non purus fermentum efficitur. Donec et semper erat. Pellentesque quis malesuada neque. Vestibulum tempor, eros eu iaculis cursus, purus ante mollis risus, vel dapibus sapien turpis tempus sapien. Phasellus sodales tortor eget tristique rhoncus.
            </p>
          </div>
        </div>
        <div className='a12'>
          <div className="a12-wrapper">
          </div>
        </div>
      </div>
  )
}

export default HomePage