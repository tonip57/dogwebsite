import React from 'react'
import './OurDogs.css'
import { useState } from 'react'
import uutisiaKuva from '../../img/koera.jpg'

const OurDogs = () => {
  const  [selectedList, setSelectedList] = useState([true, false, false])

  const dogs = [{
    name: 'Tosca',
    age: 6,
    gender: 'Narttu',
    breed: 'Chihuahua',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Fusce convallis risus sem, id faucibus ipsum convallis a.',
    image: uutisiaKuva
  },{
      name: 'Ilona',
      age: 5,
      gender: 'Narttu',
      breed: 'Chihuahua',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Fusce convallis risus sem, id faucibus ipsum convallis a.',
      image: uutisiaKuva
    },{
    name: 'Tosca',
    age: 6,
    gender: 'Narttu',
    breed: 'Chihuahua',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Fusce convallis risus sem, id faucibus ipsum convallis a.',
    image: uutisiaKuva
  },{
      name: 'Ilona',
      age: 5,
      gender: 'Narttu',
      breed: 'Chihuahua',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce convallis risus sem, id faucibus ipsum convallis a. Fusce convallis risus sem, id faucibus ipsum convallis a.',
      image: uutisiaKuva
    }]

  function handleButtonClick(index) {
    let list = [false, false, false]
    list[index] = true
    setSelectedList(list)
  }

  return (
    <div className='ourdogs-page'>
      <div className='ourdogs-togglebuttons'>
        {selectedList[0] === false && <button className='ourdogs-togglebutton' onClick={() => handleButtonClick(0)}>Koiramme</button>}
        {selectedList[0] === true && <button className='ourdogs-togglebutton-selected'>Koiramme</button>}
        {selectedList[1] === false && <button className='ourdogs-togglebutton' onClick={() => handleButtonClick(1)}>Kasvatit</button>}
        {selectedList[1] === true && <button className='ourdogs-togglebutton-selected'>Kasvatit</button>}
        {selectedList[2] === false && <button className='ourdogs-togglebutton' onClick={() => handleButtonClick(2)}>Pentueet</button>}
        {selectedList[2] === true && <button className='ourdogs-togglebutton-selected'>Pentueet</button>}
      </div>
      {selectedList[0] === true && dogs.map((dogs2) =>
        <div className='ourdogs-column'>
          <div className='ourdogs-dogcard'>
            <div className='ourdogs-dogcard-grid'>
              <div className='ourdogs-dogcard-title'>
                <div className='ourdogs-dogname'>
                  {dogs2.name}
                </div>
                <div className='ourdogs-doggender'>
                  {dogs2.gender},
                </div>
                <div className='ourdogs-dogage'>
                  {dogs2.age}v
                </div>
              </div>
              <div className='ourdogs-dogcard-text-container'>
                <div className='ourdogs-dogcard-text'>
                  {dogs2.description}
                </div>
              </div>
              <div className='ourdogs-dogcard-image-container' style={{backgroundImage:`url(${uutisiaKuva})`}}>

              </div>
            </div>
          </div>
        </div>
    )}
    {selectedList[1] === true && <div className='notfound'>Kasvatit -sivu lisätään myöhemmin!</div>}
    {selectedList[2] === true && <div className='notfound'>Pentueet -sivu lisätään myöhemmin!</div>}
    </div>
  )
}

export default OurDogs